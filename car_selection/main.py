import random
import time
import send_discord_message

cars = ["Bentley", "Porsche 992", "Porsche Cayman", "BMW M2", "Maserati GT2", "Audi GT3"]
counts = {s: 0 for s in cars}

while True:
    random_string = random.choice(cars)
    counts[random_string] += 1
    send_discord_message.send_discord_message(random_string)
    time.sleep(5)
    
    if counts[random_string] == 3:
        send_discord_message.send_discord_message(f"The Winner is: {random_string}")
        break
