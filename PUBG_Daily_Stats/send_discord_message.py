import requests
import json
import os
from dotenv import load_dotenv

# Load variables from .env file
load_dotenv()

# Get the Discord token from environment variables
discord_token = os.getenv("discord_token")


DISCORD_API = "https://discord.com/api"
PUBG_THREAD = "1283156797082964008"
LFM_API = "https://api2.lowfuelmotorsport.com/api"

# you figure discord token out
def send_discord_message(content, discord_channel=PUBG_THREAD, token: str=discord_token):
    url = f"{DISCORD_API}/v9/channels/{discord_channel}/messages"
    headers = {
        "Authorization": token,
        "Content-Type": "application/json"
    }

    data = {"content": str(content)}

    response = requests.post(url, headers=headers, data=json.dumps(data))

    if response.status_code == 200:
        response_json = response.json()
        message_id = response_json.get("id")
        print("Request successful! Message ID:", message_id)
        return message_id
    else:
        print("Request failed with status code:", response.status_code, response.text)