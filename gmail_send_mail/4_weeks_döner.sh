#!/bin/bash

cd /home/luca/pst-scripts/gmail_send_mail

# Path to the counter file
COUNTER_FILE="counter.txt"

# Read the current counter value
COUNTER=$(cat "$COUNTER_FILE")

# Increment the counter
((COUNTER++))

# Check if the counter has reached 4
if [ "$COUNTER" -eq 4 ]; then
    # Trigger the desired action
    echo "Triggering the task..."

    # Run your command here
    python3 send_mail.py 'Es ist Zeit Döner zu essen am Sonntag, vier Wochen sind um!'

    # Reset the counter
    COUNTER=0
fi

# Save the updated counter back to the file
echo "$COUNTER" > "$COUNTER_FILE"
