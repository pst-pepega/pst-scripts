#!/bin/bash

cd /home/luca/pst-scripts/bop_notifications_gt4
python3 /home/luca/pst-scripts/bop_notifications_gt4/main.py > bop_notification.log
current_date=$(date +"%Y-%m-%d %H:%M:%S")
git add bop_scores.csv laps_data.csv all_bop.json
git commit -m "new gt4 bop data [$current_date]"
