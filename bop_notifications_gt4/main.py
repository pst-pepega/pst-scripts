import bop
import time
import logging
import os
import send_discord_message
import csv
import bop_score
import save_all_bop
import time

def countdown(seconds):
    while seconds > 0:
        print(f"Time remainning: {seconds} seconds")
        time.sleep(1)
        seconds -= 1


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
file_handler = logging.FileHandler('log.log')
file_handler.setLevel(logging.INFO)
logger.addHandler(file_handler)
LFM_API = "https://api2.lowfuelmotorsport.com/api"

# Specify the file name
csv_file_name = 'laps_data.csv'
temp_file = "temp.csv"

# Specify the header names
track_list = bop.get_all_tracks()
#track_list = ["Nordschleife"]
bop_scores = dict()
all_bop = dict()

send_discord_message.send_discord_message("Test to see that bop notifications is running", 1240300121254531072)
with open(temp_file, mode='a', newline='') as file:
# Create a CSV writer object
    csv_writer = csv.writer(file)
    csv_writer.writerow(["car_id", "track_id", "car_name", "lap", "bop", "bop_raw", "source"])
logger.info(track_list)
for track in track_list:
    logger.info(track)
    result = ""
    track_id = bop.get_track_id(track)
    time.sleep(5)
    if "nordschleife" in track.lower():
        track_id = bop.get_track_id("nordschleife")

    try:
        relevant_laps, others_laps = bop.get_bop(track)
    except:
        #countdown(600)
        relevant_laps, others_laps = bop.get_bop(track)
    
    current_time = time.time()
    if relevant_laps:
        for lap in relevant_laps:
            car_id = lap.get('car_id')
            car = lap.get('car_name')
            try:
                old_bop = bop.search_data(str(car_id), str(track_id)).get('bop')
            except:  # noqa: E722
                old_bop = "N/A"
                
            new_bop = lap.get('bop')
            if old_bop == "N/A":
                difference = "NEW"
            else:
                difference = int(new_bop) - int(old_bop)

            if old_bop == "N/A":
                if int(new_bop) > 0:
                    result += f"- {car} BOP changed from {old_bop} to {new_bop} [Δ{difference}] on {track}\n"
                else:
                    result += f"+ {car} BOP changed from {old_bop} to {new_bop} [Δ{difference}] on {track}\n"
            elif int(old_bop) != int(new_bop):
                # insert discord message
                logger.info(f"{car} BOP changed from {old_bop} to {new_bop} on {track}")
                if difference > 0:
                    result += f"- {car} BOP changed from {old_bop} to {new_bop} [Δ{difference}] on {track}\n"
                elif difference < 0:
                    result += f"+ {car} BOP changed from {old_bop} to {new_bop} [Δ{difference}] on {track}\n"

            else:
                logger.info(f"### {car} BOP changed from {old_bop} to {new_bop} on {track}")

    if others_laps:
        for lap in others_laps:
            car_id = lap.get('car_id')
            car = lap.get('car_name')

            if int(lap.get('bop')) > 0:
                try:
                    old_bop =  bop.search_data(str(car_id), str(track_id)).get('bop')
                except:  # noqa: E722
                    old_bop = "N/A"

                new_bop = lap.get('bop')
                if old_bop == "N/A":
                    difference = "NEW"
                else:
                    difference = int(new_bop) - int(old_bop)

                if old_bop == "N/A":
                    if int(new_bop) > 0:
                        result += f"- {car} BOP changed from {old_bop} to {new_bop} [Δ{difference}] on {track}\n"
                    else:
                        result += f"+ {car} BOP changed from {old_bop} to {new_bop} [Δ{difference}] on {track}\n"
                elif int(old_bop) != int(new_bop):
                    # insert discord message
                    logger.info(f"{car} BOP changed from {old_bop} to {new_bop} on {track}")
                    if difference > 0:
                        result += f"- {car} BOP changed from {old_bop} to {new_bop} [Δ{difference}] on {track}\n"
                    elif difference < 0:
                        result += f"+ {car} BOP changed from {old_bop} to {new_bop} [Δ{difference}] on {track}\n"
                else:
                    logger.info(f"### {car} BOP changed from {old_bop} to {new_bop} on {track}")
    
    try:
        old_score = new_score = float(bop_score.find_bop_score(track_id))
        logger.info(old_score)
    except:
        old_score = 0
    
    try:
        new_score = float(bop_score.get_score(track))
        logger.info(new_score)
    except:
        new_score = 0

    if result != "":
        try:
            difference_score = round(new_score-old_score, 3)
        except:
            difference_score = 0
        message_id = send_discord_message.send_discord_message(f"Updated BOP as of: <t:{int(current_time)}>\n```diff\n{result}\n``` BOP-Score sponsored by Dennis Schöps: {new_score} [{difference_score}]")
        #send_discord_message.send_crosspost(message_id)
        time.sleep(1)
        #FOCH_DISCORD = 1204885962161651742
        #message_id = send_discord_message.send_discord_message(f"```diff\n{result}\n``` BOP-Score sponsored by Dennis Schöps: {new_score} [{difference_score}]", FOCH_DISCORD)
        #send_discord_message.send_crosspost(message_id)
        logger.info(result)
        
    countdown(30)

    bop_scores[track_id] = new_score

    if relevant_laps:
        bop.write_bop_to_csv(relevant_laps, others_laps, track_id, temp_file)            
        save_all_bop.add_to_all_bop(relevant_laps, others_laps, all_bop)

for car_name in all_bop:
    all_bop[car_name] = round(all_bop[car_name] / len(track_list), 3)

all_bop = dict(sorted(all_bop.items(), key=lambda item: item[1], reverse=True))

save_all_bop.save_all_bop(all_bop, "all_bop_temp.json")
all_bop_string = save_all_bop.return_all_bop_value(all_bop)
if all_bop_string != "**Average BOP**```diff\n```":
    send_discord_message.send_discord_message(all_bop_string)
os.remove("all_bop.json")
os.rename("all_bop_temp.json", "all_bop.json")

os.remove(csv_file_name)
os.rename(temp_file, csv_file_name)
csv_file_path = "bop_scores.csv"

with open(csv_file_path, 'w', newline='') as csvfile:
    fieldnames = ['track_id', 'bop_score']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    writer.writeheader()

    for track_id, bop_score in bop_scores.items():
        writer.writerow({'track_id': track_id, 'bop_score': bop_score})

logger.info(f"CSV file '{csv_file_path}' has been created.")