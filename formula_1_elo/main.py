import requests
import csv
import math
import time
from operator import itemgetter
import csv

def read_csv(filename):
    data = []
    with open(filename, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            data.append(row)
    return data

def csv_to_dict(csv_file = "elo_data.csv"):
    my_dict = {}
    with open(csv_file, mode='r') as file:
        reader = csv.reader(file)
        next(reader)  # Skip the header row
        for row in reader:
            key = row[0]  # Assuming first column is the key
            value = float(row[-1])  # Assuming last column is the value
            my_dict[key] = value
    return my_dict


def write_csv(filename, data):
    #print("writing data.")
    with open(filename, 'w', newline='') as csvfile:
        fieldnames = data[0].keys() if data else []
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(data)


def add_race(filename, race_name, all_drivers):
    data = read_csv(filename)
    #print("adding race.")
    
    # If CSV file is empty, create 'driver' column
    headers = list(data[0].keys()) if data else ['driver']
    
    # Add new race column to header
    headers.append(race_name)
    
    # Get the previous race name
    previous_race = headers[-2] if len(headers) > 1 else None
    
    # Add drivers from all_drivers if not already in CSV
    for driver in all_drivers.keys():
        if driver not in [row['driver'] for row in data]:
            data.append({'driver': driver})  # Add driver to data
    
    # Add Elo ratings for each driver to the new race column
    for driver_info in data:
        driver = driver_info['driver']
        if driver in all_drivers:
            elo = all_drivers[driver]
            if previous_race and driver_info.get(previous_race) != elo:
                driver_info[race_name] = elo
            elif previous_race is None:
                driver_info[race_name] = elo
            else:
                driver_info[race_name] = ''  # Empty if Elo is the same as previous race
        else:
            driver_info[race_name] = ''  # Empty for drivers not in the race
    
    write_csv(filename, data)

def calculate_elo_gain(position, drivers_count, driver_elo, other_elo):
    rBase = 1600 / math.log(2)
    elogain = 0

    for y in range(1, drivers_count+1):    
        elogain = 0
        fudgeFactor = ((drivers_count - 0) / 2 - y) / 100    
        for x in other_elo:
            x = float(x)
            if (x != driver_elo):
                a = (1 - math.exp(-driver_elo / rBase)) * math.exp(-x / rBase)
                b = (1 - math.exp(-x / rBase)) * math.exp(-driver_elo / rBase)
                c = (1 - math.exp(-driver_elo / rBase)) * math.exp(-x / rBase)
                score = a / (b + c)
                elogain = elogain + score
        
        elogain = (drivers_count - y - elogain - fudgeFactor) * 200 / drivers_count       

        if y == position:
            return elogain


def update_driver_elo(position, drivers, driver_name, race_name, drivers_elo):
    drivers_count = len(drivers)
    driver_elo = drivers_elo[driver_name][-1]
    elo_gain = calculate_elo_gain(position, drivers_count, driver_elo, [elo for elo in drivers_elo.values() if elo != drivers_elo[driver_name][-1]])
    drivers_elo[driver_name].append(driver_elo + elo_gain)

    # Append the driver's Elo rating to the CSV file
    with open('elo_ratings.csv', mode='a') as elo_file:
        elo_writer = csv.writer(elo_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        elo_writer.writerow([driver_name, race_name, drivers_elo[driver_name][-1]])


def get_race():
    base_url = "http://ergast.com/api/f1"
    all_drivers = csv_to_dict()
    drivers_elo = {}
    seasons = range(1971, 2025)
    
    # Get the list of all races for the season
    for season_year in seasons:
        response = requests.get(f"{base_url}/{season_year}.json")
        races_data = response.json()["MRData"]["RaceTable"]["Races"]
        races = len(races_data)
        counter = 0
        for race in races_data:
            counter += 1
            print(f"{counter}/{races}")
            print()
            time.sleep(10)
            race_elo = []
            race_drivers = []
            race_name = race["raceName"]
            race_date = race["date"]

            # Get the list of all results for the race
            response = requests.get(f"{base_url}/{season_year}/{race['round']}/results.json")
            results_data = response.json()["MRData"]["RaceTable"]["Races"][0]["Results"]
            #print(len(results_data))
        
            for result in results_data:
                driver_name = result["Driver"]["givenName"] + " " + result["Driver"]["familyName"]
                position = int(result["position"])

                if driver_name not in all_drivers.keys():
                    all_drivers[driver_name] = 1500
                    #print(f"{driver_name} not in all_drivers")
                
                race_elo.append(all_drivers[driver_name])
                race_drivers.append(driver_name)
                #print(race_elo)
                time.sleep(1)

            #print(race_elo)
            for result in results_data:
                driver_name = result["Driver"]["givenName"] + " " + result["Driver"]["familyName"]
                position = int(result["position"])
                
                #print(f"{position}. {driver_name}")
                elo = calculate_elo_gain(position, len(results_data), all_drivers[driver_name], race_elo)
                #print(elo)
                all_drivers[driver_name] = all_drivers[driver_name] + elo

            race_name = f"{race_name} {season_year}"
            add_race('elo_data.csv', race_name, all_drivers)
            

        # Sort dictionary by value in descending order
        temp = sorted(all_drivers.items(), key=itemgetter(1), reverse=True) 

        # Print top 5 drivers
        print(f"# {season_year}")
        print("Top 10 Drivers:")
        for i in range(10):
            driver, points = temp[i]
            print(f"{driver}: {points}")
            time.sleep(5)

def get_specific_race(season_year: int, round: int):
    base_url = "http://ergast.com/api/f1"
    all_drivers = csv_to_dict()
    drivers_elo = {}
    
    # Get the list of all races for the season
     
    response = requests.get(f"{base_url}/{season_year}.json")
    races_data = response.json()["MRData"]["RaceTable"]["Races"]
    races = len(races_data)
    counter = 0
    for race in races_data:
        counter += 1
        if counter != round:
            continue
        print(f"{counter}/{races}")
        print()
        time.sleep(10)
        race_elo = []
        race_drivers = []
        race_name = race["raceName"]
        race_date = race["date"]

        # Get the list of all results for the race
        response = requests.get(f"{base_url}/{season_year}/{round}/results.json")
        results_data = response.json()["MRData"]["RaceTable"]["Races"][0]["Results"]
        #print(len(results_data))
    
        for result in results_data:
            driver_name = result["Driver"]["givenName"] + " " + result["Driver"]["familyName"]
            position = int(result["position"])

            if driver_name not in all_drivers.keys():
                all_drivers[driver_name] = 1500
                #print(f"{driver_name} not in all_drivers")
            
            race_elo.append(all_drivers[driver_name])
            race_drivers.append(driver_name)
            #print(race_elo)
            time.sleep(1)

        #print(race_elo)
        for result in results_data:
            driver_name = result["Driver"]["givenName"] + " " + result["Driver"]["familyName"]
            position = int(result["position"])
            
            #print(f"{position}. {driver_name}")
            elo = calculate_elo_gain(position, len(results_data), float(all_drivers[driver_name]), race_elo)
            #print(elo)
            all_drivers[driver_name] = all_drivers[driver_name] + elo

        race_name = f"{race_name} {season_year}"
        add_race('elo_data.csv', race_name, all_drivers)
        

    # Sort dictionary by value in descending order
    temp = sorted(all_drivers.items(), key=itemgetter(1), reverse=True) 

    # Print top 5 drivers
    print(f"# {season_year}")
    print("Top 5 Drivers:")
    for i in range(10):
        driver, points = temp[i]
        print(f"{driver}: {points}")
        time.sleep(5)

if __name__ == "__main__":
    #drivers_elo = get_all_race_results()
    #get_specific_race(1970, 13)
    get_race()