import requests
import csv

# Function to get race data from the API
def fetch_race_data(race_id):
    url = f"https://api2.lowfuelmotorsport.com/api/race/{race_id}"
    response = requests.get(url)
    if response.status_code == 200:
        return response.json()
    else:
        print(f"Failed to fetch data for race {race_id}")
        return None

# Initialize dictionary to store driver data
drivers = {}

# Function to add/update driver in the dictionary
def update_driver(driver_name, car_name, points, race_idx):
    if driver_name not in drivers:
        # Initialize a new driver entry with empty points for earlier races
        drivers[driver_name] = {
            'car_name': car_name,
            'points': ['' for _ in range(race_idx)] + [points]
        }
    else:
        # Update the driver's points list for the race
        current_points = drivers[driver_name]['points']
        # Ensure the points list has enough entries
        while len(current_points) < race_idx:
            current_points.append('')
        current_points.append(points)

# Main function to process races
def process_races(start_race_id, end_race_id):
    for race_id in range(start_race_id, end_race_id + 1):
        race_data = fetch_race_data(race_id)
        if race_data is None:
            continue

        # Process all available splits
        try:
            splits = race_data['race_results_splits']
            
            for split in splits:
                if 'GT3' in split and 'OVERALL' in split['GT3']:
                    overall = split['GT3']['OVERALL']
                    
                    for entry in overall:
                        driver_name = f"{entry['vorname']} {entry['nachname']}"
                        car_name = entry['car_name']
                        points = entry['points']
                        
                        update_driver(driver_name, car_name, points, race_id - start_race_id)
        
        except KeyError:
            print(f"Race {race_id} does not have expected data structure.")

# Function to save data to CSV
def save_to_csv(filename):
    # Determine the maximum number of races processed (i.e., the longest points list)
    max_races = max(len(data['points']) for data in drivers.values())
    
    # Prepare the CSV header
    header = ['Driver', 'Car Name'] + [f'Race {i + 1}' for i in range(max_races)]
    
    # Write data to CSV
    with open(filename, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(header)
        
        # Write each driver's data
        for driver_name, data in drivers.items():
            row = [driver_name, data['car_name']] + data['points'] + [''] * (max_races - len(data['points']))
            writer.writerow(row)

            
# Start processing races
process_races(131908, 131919)

# Save the result to CSV
save_to_csv('race_results.csv')

print("Data saved to race_results.csv")
