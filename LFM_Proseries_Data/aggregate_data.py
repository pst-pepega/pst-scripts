import pandas as pd

# Load the initial data
df = pd.read_csv("race_results.csv")

# Group data by 'vorname', 'nachname', and 'user_id' and aggregate statistics
aggregated_data = df.groupby(['vorname', 'nachname', 'user_id']).agg(
    Races=('position', 'count'),
    Wins=('position', lambda x: (x == 1).sum()),
    Podiums=('position', lambda x: (x <= 3).sum()),
    Pole_Positions=('pole_position', lambda x: x.sum()),
    Fastest_Laps=('fastest_lap', lambda x: x.sum()),
    Top5s=('position', lambda x: (x <= 5).sum()),
    Top10s=('position', lambda x: (x <= 10).sum()),
    Avg_Finishing=('position', 'mean'),
    Avg_Qualifying=('starting_position', 'mean'),
    Points=('points', 'sum'),
    Points_per_Race=('points', 'mean'),
    Winrate=('position', lambda x: (x == 1).sum() / x.count() if x.count() > 0 else 0),
    Podiumrate=('position', lambda x: (x <= 3).sum() / x.count() if x.count() > 0 else 0),
    Pole_Position_Rate=('pole_position', lambda x: x.sum() / x.count() if x.count() > 0 else 0),
    Fastest_Lap_Rate=('fastest_lap', lambda x: x.sum() / x.count() if x.count() > 0 else 0),
    Sum_Elo_Gain=('ratingGain', 'sum'),
    Sum_SR_Gain=('sr_change', 'sum'),
    Sum_IPs=('incidents', 'sum'),
    IP_per_Race=('incidents', 'mean'),
    average_SOF=('sof', 'mean')
).reset_index()

# Calculate Q -> R (Qualifying to Race improvement) as Avg_Qualifying - Avg_Finishing
aggregated_data['Q_to_R'] = aggregated_data['Avg_Qualifying'] - aggregated_data['Avg_Finishing']

# Save the result to a new CSV file
aggregated_data.to_csv("lfm_proseries_data.csv", index=False)

print("Aggregated data saved to lfm_proseries_data.csv")
