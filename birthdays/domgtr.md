Calling all Nissan enthusiasts!   Today we celebrate our fellow member, <@294976347695218689> from England.

Dom's love for Nissan's engineering, especially the GTR, is legendary in our community. We appreciate his passion for precision driving and getting the most out of his ride.

Here's to a birthday filled with perfect corners and pure Nissan power!