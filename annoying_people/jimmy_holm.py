import random
from starters import starters
import send_discord_message

def print_random_starter():
    # Randomly decide whether to print a starter (25% chance)
    if random.random() <= 0.50:
        # Pick a random starter from the list and replace "Jimmy" with the Discord mention
        starter = random.choice(starters)
        starter = f"Hey <@122020644752850946>. {starter}"
        print(starter)
        send_discord_message.send_discord_message(starter, 1015601499117727856)
    else:
        print("No conversation starter today.")

if __name__ == "__main__":
    print_random_starter()
