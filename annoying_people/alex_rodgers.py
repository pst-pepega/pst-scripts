import send_discord_message
import json
import random
import re
import twitch_days

ALEX_RODGERS = 204246623537201152
ALEX_RODGERS_GENERAL_CHAT = 448937940031373344
PST_GENERAL_CHAT = 1015601499117727856

# Open the file in read mode
# with open("alex_rodgers_counter.txt", "r") as file:
#     # Read the number from the file
#     num = int(file.read())

with open('alex_rodgers_direct_messages.json') as f:
    direct = json.load(f)

with open('alex_rodgers_indirect_messages.json') as f:
    indirect = json.load(f)

random_indirect = random.choice([item['sentence'] for item in indirect])
random_direct = random.choice([item['sentence'] for item in direct])

# # Add 1 to the number
# num += 1

# # Open the file in write mode
# with open("alex_rodgers_counter.txt", "w") as file:
#     # Write the new number to the file
#     file.write(str(num))

num = twitch_days.get_alex_days()

if num > 0:

    random_direct = re.sub("\$\DAYS", f"{num} days", random_direct)
    random_indirect = re.sub("\$\DAYS", f"{num} days", random_indirect)
    random_direct = random_direct.replace("$NAME", f"<@{ALEX_RODGERS}> ")
    #random_direct = re.sub("$NAME.", f"<@{ALEX_RODGERS}> ", random_direct)
    message = f"{random_indirect}, {random_direct}"
    message = message.replace("  ", " ").replace(" , ", ", ").replace(".,",",")
    print(message)

    if num < 50:
        send_discord_message.send_discord_message(message, ALEX_RODGERS_GENERAL_CHAT)
    elif num > 50 and num <= 100 and num % 2 == 0:
        send_discord_message.send_discord_message(message, ALEX_RODGERS_GENERAL_CHAT)
    elif num > 100 and num <= 200 and num % 3 == 0:
        send_discord_message.send_discord_message(message, ALEX_RODGERS_GENERAL_CHAT)
    elif num > 200 and num <= 350 and num % 4 == 0:
        send_discord_message.send_discord_message(message, ALEX_RODGERS_GENERAL_CHAT)
    elif num > 350 and num <= 550 and num % 5 == 0:
        send_discord_message.send_discord_message(message, ALEX_RODGERS_GENERAL_CHAT)
    elif num > 550 and num <= 800 and num % 6 == 0:
        send_discord_message.send_discord_message(message, ALEX_RODGERS_GENERAL_CHAT)
    elif num > 800 and num % 7 == 0:
        send_discord_message.send_discord_message(message, ALEX_RODGERS_GENERAL_CHAT)
    else:
        send_discord_message.send_discord_message("Today, we did not remind Axel Rodgers to stream again.", PST_GENERAL_CHAT)

else:
    message = twitch_days.get_alex_data()
    message = message.replace("Alex", f"<@{ALEX_RODGERS}>")
    send_discord_message.send_discord_message(message, ALEX_RODGERS_GENERAL_CHAT)
    send_discord_message.send_discord_message("Alex streamed in the last 24h, wtf", PST_GENERAL_CHAT)
