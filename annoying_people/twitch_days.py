import requests
from datetime import datetime, timedelta

def get_teemu_days():
    # Set the URL
    url = "https://sullygnome.com/api/tables/channeltables/streams/365/32557836/%20/1/1/desc/0/50"

    # Add headers to mimic a browser request
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36"
    }

    # Fetch the data from the URL
    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        data = response.json()
        
        # Access startDateTime and other data
        start_date_time = data['data'][0]['startDateTime']
        length = data['data'][0]['length']
        
        # Parse the start date time string to a datetime object
        start_datetime = datetime.fromisoformat(start_date_time[:-1]) + timedelta(minutes=int(length)) # Remove 'Z' and convert
        current_datetime = datetime.utcnow()  # Get the current UTC datetime
        
        # Calculate the difference in days
        days_difference = (current_datetime - start_datetime).days

        return days_difference
    else:
        print(f"Error fetching data: {response.status_code}")


def get_teemu_data():
    # Set the URL
    url = "https://sullygnome.com/api/tables/channeltables/streams/365/32557836/%20/1/1/desc/0/50"

    # Add headers to mimic a browser request
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36"
    }

    # Fetch the data from the URL
    response = requests.get(url, headers=headers)

    message = ""

    if response.status_code == 200:
        data = response.json()
        
        # Access startDateTime and other data
        start_date_time = data['data'][0]['startDateTime']
        length = data['data'][0]['length']
        follower_gain = data['data'][0]['followergain']
        avg_viewers = data['data'][0]['avgviewers']
        max_viewers = data['data'][0]['maxviewers']
        view_minutes = data['data'][0]['viewminutes']
        
        message = f"Hey folks, let me tell you, what a tremendous stream we had yesterday with Teemu! It was amazing, truly amazing. We’re talking about a fantastic length of {length} minutes of pure entertainment, folks. People loved it! We made {follower_gain} new friends—great people, the best! There were {avg_viewers} people watching that Bentley domination, just incredible, could have been as many as {max_viewers}! And let me tell you, we racked up {view_minutes} minutes of enjoyment. Everybody’s talking about it. Just tremendous!"
        return message
    else:
        print(f"Error fetching data: {response.status_code}")


def get_alex_days():
    # Set the URL
    url = "https://sullygnome.com/api/tables/channeltables/streams/365/53176/%20/1/1/desc/0/50"

    # Add headers to mimic a browser request
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36"
    }

    # Fetch the data from the URL
    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        data = response.json()
        
        # Access startDateTime
        try:
            start_date_time = data['data'][0]['startDateTime']
            length = data['data'][0]['length']
        except:
            start_date_time = "2022-06-25T19:57:00Z"

        # Parse the start date time string to a datetime object
        start_datetime = datetime.fromisoformat(start_date_time[:-1]) + timedelta(minutes=int(length)) # Remove 'Z' and convert
        current_datetime = datetime.utcnow()  # Get the current UTC datetime
        
        # Calculate the difference in days
        days_difference = (current_datetime - start_datetime).days
        
        return days_difference
    else:
        print(f"Error fetching data: {response.status_code}")


def get_alex_data():
    # Set the URL
    url = "https://sullygnome.com/api/tables/channeltables/streams/365/53176/%20/1/1/desc/0/50"

    # Add headers to mimic a browser request
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36"
    }

    # Fetch the data from the URL
    response = requests.get(url, headers=headers)

    message = ""

    if response.status_code == 200:
        data = response.json()
        
        # Access startDateTime and other data
        start_date_time = data['data'][0]['startDateTime']
        length = data['data'][0]['length']
        follower_gain = data['data'][0]['followergain']
        avg_viewers = data['data'][0]['avgviewers']
        max_viewers = data['data'][0]['maxviewers']
        view_minutes = data['data'][0]['viewminutes']
        
        message = f"Hey folks, let me tell you, what a tremendous stream we had yesterday with Alex! It was amazing, truly amazing. We’re talking about a fantastic length of {length} minutes of pure entertainment, folks. People loved it! We made {follower_gain} new friends—great people, the best! There were {avg_viewers} people watching that domination, just incredible, could have been as many as {max_viewers}! And let me tell you, we racked up {view_minutes} minutes of enjoyment. Everybody’s talking about it. Just tremendous!"
        return message
    else:
        print(f"Error fetching data: {response.status_code}")

        
def get_schubert_days():
    # Set the URL
    url = "https://sullygnome.com/api/tables/channeltables/streams/365/25687545/%20/1/1/desc/0/50"

    # Add headers to mimic a browser request
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36"
    }

    # Fetch the data from the URL
    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        data = response.json()
        
        # Access startDateTime and other data
        start_date_time = data['data'][0]['startDateTime']
        length = data['data'][0]['length']
        
        # Parse the start date time string to a datetime object
        start_datetime = datetime.fromisoformat(start_date_time[:-1]) + timedelta(minutes=int(length)) # Remove 'Z' and convert
        current_datetime = datetime.utcnow()  # Get the current UTC datetime
        
        # Calculate the difference in days
        days_difference = (current_datetime - start_datetime).days

        return days_difference
    else:
        print(f"Error fetching data: {response.status_code}")


def get_schubert_data():
    # Set the URL
    url = "https://sullygnome.com/api/tables/channeltables/streams/7/25687545/%20/1/1/desc/0/50"

    # Add headers to mimic a browser request
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36"
    }

    # Fetch the data from the URL
    response = requests.get(url, headers=headers)

    message = ""

    if response.status_code == 200:
        data = response.json()
        
        # Access startDateTime and other data
        start_date_time = data['data'][0]['startDateTime']
        length = data['data'][0]['length']
        follower_gain = data['data'][0]['followergain']
        avg_viewers = data['data'][0]['avgviewers']
        max_viewers = data['data'][0]['maxviewers']
        view_minutes = data['data'][0]['viewminutes']
        
        message = f"Hey folks, let me tell you, what a tremendous stream we had yesterday with Hubert! It was amazing, truly amazing. We’re talking about a fantastic length of {length} minutes of pure entertainment, folks. People loved it! We made {follower_gain} new friends—great people, the best! There were {avg_viewers} people watching that domination, just incredible, could have been as many as {max_viewers}! And let me tell you, we racked up {view_minutes} minutes of enjoyment. Everybody’s talking about it. Just tremendous!"
        return message
    else:
        print(f"Error fetching data: {response.status_code}")