import requests
import next_race
import send_discord_message
import sys
import logging

# Role IDs
GT3_3000 = 1267414304848613447
GT3_4000 = 1267414351002734695
SPRINT_3000 = 1267414405554110484
SPRINT_4000 = 1267414433035063337
GT4_3000 = 1267414451829604437
GT4_2500 = 1267414483391877161

row = next_race.get_race_id()

if row != None:
    race_id = row['id']
    url = f"https://api2.lowfuelmotorsport.com/api/race/{race_id}"
    
    response = requests.get(url)
    json_data = response.json()
    SOF = json_data['sof']
    message = f"The SOF for the next {sys.argv[1].upper()} Race at {row['Track']} will be: **{SOF}**\n\nWet/Dry/Mixed: {round(float(row['Wet Chance'])*100,3)}%/{round(float(row['Dry Chance'])*100,3)}%/{round(float(row['Mixed Chance'])*100,3)}% and Average Temps of {row['Avg Temp']}"
    
    if SOF >= 2500:
        if sys.argv[1] == "gt4":
            message += f"<@&{GT4_2500}>"
            
            logging.info(message)
            send_discord_message.send_discord_message(message, 1262430846032285717)


    if SOF >= 3000:
        if sys.argv[1] == "gt3":
            message += f"<@&{GT3_3000}>"
        elif sys.argv[1] == "sprint":
            message += f"<@&{SPRINT_3000}>"
        elif sys.argv[1] == "gt4":
            message += f"<@&{GT4_3000}>"
            
        logging.info(message)
        send_discord_message.send_discord_message(message, 1262430846032285717)
    
    if SOF >= 4000:
        if sys.argv[1] == "gt3":
            message += f"<@&{GT3_4000}>"
        elif sys.argv[1] == "sprint":
            message += f"<@&{SPRINT_4000}>"
            
        logging.info(message)
        send_discord_message.send_discord_message(message, 1262430846032285717)


