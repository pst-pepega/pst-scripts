import csv
from datetime import datetime, timedelta
import sys
import requests
import logging
import send_discord_message
import car_lookup
import time
import emoji
import car_counts

FORGOT = False
SIGN_UPS_OPEN = False

def check_elo(id: int):
    url = f"https://api2.lowfuelmotorsport.com/api/users/getUserData/{id}?with_achievements=true"
    logging.info(f"Checking Elo for {url}\n")

    try:
        response = requests.get(url)
        json_data = response.json()
        elo = json_data['rating_by_sim'][0]['rating']
    except:
        elo = 0

    logging.info(f"It's {elo} elo.")
    return elo

ids_to_check = [
    (6623, "Jack", "Alexander"),
    (2683, "Samir", "Foch"),
    (40682, "Zach", "Doran"),
    (111486, "Sam", "Gaskell"),
    (46036, "Erik", "Gesell"),
    (5553, "Jimmy",  "Holm"),
    (40102, "Kevin", "Taddei"),
    (25889, "Fabrizio", "Campanella"),
    (89924, "Fabio", "Zerbin"),
    (15373, "Jonathan", "Clifford"),
    (8604, "Eamonn", "Murphy"),
    (2771, "Ryan", "Cooper"),
    (5913, "Teemu", "Karppinen"),
    (2761, "Vojtech", "Neuwirth"),
    (8171, "Joshua", "Grüning"),
    (1229, "Justin", "Stubenrauch"),
    (7300, "Luke", "Whitehead"),
    (167800, "Shae", "O'Sullivan"),
    (29238, "Laurids", "Lhyne Iversen"),
    (8357, "Grantas", "Kareckas"),
    (11724, "James", "Baldwin")
]

logging.basicConfig(stream=sys.stdout, level=logging.INFO)

def find_drivers_split1(race_id):
    result_in = ""
    result_out_signed = "**Not Signed In**:\n"
    result_out_elo = "**Not Enough Elo**:\n"
    cut_off = 0
    url = f"https://api2.lowfuelmotorsport.com/api/race/{race_id}"

    try:
        response = requests.get(url)
        json_data = response.json()

        if 'participants' in json_data:
            entrylist = json_data['participants']
            if 'entries' in entrylist:
                entries = entrylist['entries']
                max_drivers = (len(entries))
                cut_off = entries[max_drivers-1]['elo']
                print(cut_off)
                for entry_info in entries:
#                    driver = entry_info['drivers'][0]
                    first_name = entry_info['vorname']
                    last_name = entry_info['nachname']
                    raceNumber = entry_info['raceNumber']
                    car_id = entry_info['car_model']
                    id = entry_info['user_id']
                    car_name = car_lookup.Car_lookup(car_id)
                    if any(id == ids[0] for ids in ids_to_check):
                    #if id in ids_to_check[0]:
                        car_name = f"({car_name})"
                        raceNumber = f"[{raceNumber}]" 
                        result_in += (f"{first_name:<{15}}{last_name:<{15}}{car_name:<{15}}{raceNumber:<{15}}\n")
                    else:
                        logging.info(f"Driver {first_name} {last_name} is not in the provided list.")
            else:
                logging.info("No entries found for this race.")
        else:
            logging.info("No entrylist found in the JSON data.")

        for ids in ids_to_check:
            if ids[1] not in result_in:
                elo_difference = cut_off - check_elo(ids[0])
                if elo_difference < 0:
                    result_out_signed += f"{ids[1]} {ids[2]}, "
                else:
                    result_out_elo += f"{ids[1]} {ids[2]} [**{elo_difference}**], "

    except Exception as e:
        logging.info(f"An error occurred: {e}")
    return (result_in, result_out_signed, result_out_elo, max_drivers, cut_off)


def find_drivers_split2(race_id, split1_elo):
    result_in = ""
    result_out_signed = "**Not Signed In**:\n"
    result_out_elo = "**Not Enough Elo**:\n"
    cut_off = 0
    url = f"https://api2.lowfuelmotorsport.com/api/race/{race_id}"

    try:
        response = requests.get(url)
        json_data = response.json()["splits"]

        if 'participants' in json_data:
            entrylist = json_data['participants'][1]
            if 'entries' in entrylist:
                entries = entrylist['entries']
                max_drivers = (len(entries))
                cut_off = entries[max_drivers-1]['elo']
                print(cut_off)
                for entry_info in entries:
#                    driver = entry_info['drivers'][0]
                    first_name = entry_info['vorname']
                    last_name = entry_info['nachname']
                    raceNumber = entry_info['raceNumber']
                    car_id = entry_info['car_model']
                    id = entry_info['user_id']
                    car_name = car_lookup.Car_lookup(car_id)
                    if any(id == ids[0] for ids in ids_to_check):
                    #if id in ids_to_check[0]:
                        car_name = f"({car_name})"
                        raceNumber = f"[{raceNumber}]" 
                        result_in += (f"{first_name:<{15}}{last_name:<{15}}{car_name:<{15}}{raceNumber:<{15}}\n")
                    else:
                        logging.info(f"Driver {first_name} {last_name} is not in the provided list.")
            else:
                logging.info("No entries found for this race.")
        else:
            logging.info("No entrylist found in the JSON data.")

        for ids in ids_to_check:
            if ids[1] not in result_in:
                elo = check_elo(ids[0])
                if elo > split1_elo:
                    continue
                else:
                    elo_difference = cut_off - elo
                    if elo_difference < 0:
                        result_out_signed += f"{ids[1]} {ids[2]}, "
                    else:
                        result_out_elo += f"{ids[1]} {ids[2]} [**{elo_difference}**], "

    except Exception as e:
        logging.info(f"An error occurred: {e}")
    return (result_in, result_out_signed, result_out_elo, max_drivers, cut_off)


def countdown(seconds):
    while seconds > 0:
        print(f"Time remainning: {seconds} seconds")
        time.sleep(1)
        seconds -= 1

def check_time_and_sleep(FORGOT):
    now = datetime.now()
    day = now.strftime("%A")
    hour = now.hour
    minute = now.minute


    if FORGOT:
        send_discord_message.send_discord_message(f"You forgot to cancel the script, dumbfuck.")
        countdown(24 * 3600)
        return True

    else:

        if day != "Wednesday" and day != "Thursday":
            logging.info("Not Wednesday nor Thursday.")
            countdown(1 * 3600)
            return False

        elif day == "Wednesday" and hour < 19:
            logging.info("Wednesday before 19:00")
            countdown(1 * 3600)
            return False

        elif day == "Wednesday" and hour == 19:
            logging.info("Wednesday 19:xx, setting sign ups open to true, in 1h will send message.")
            send_discord_message.send_discord_message("Get ready guys.")
            countdown(1 * 3600)
            return False

        elif day == "Wednesday" and (hour == 20 or (hour > 20 and hour < 24)):
            hours = 6
            send_discord_message.send_discord_message(f"See you in {hours} hours.")
            countdown(hours * 3600)   
            return False
        
        elif day == "Thursday":
            if 0 <= hour < 8:
                hours = 6
                send_discord_message.send_discord_message(f"See you in {hours} hours.")
                countdown(hours * 3600 - hours*60)
                return False

            elif 8 <= hour < 14:
                hours = 3
                send_discord_message.send_discord_message(f"See you in {hours} hours.")
                countdown(hours * 3600 - hours*60)
                return False

            elif 14 <= hour < 17:
                hours = 1.5
                send_discord_message.send_discord_message(f"See you in {hours} hours.")
                countdown(hours * 3600 - hours*60)
                return False

            elif 17 <= hour < 19:
                send_discord_message.send_discord_message(f"See you in ½ hours.")
                countdown(0.5 * 3600 - 30)
                return False

            elif 19 <= hour < 20:
                send_discord_message.send_discord_message(f"See you in ¼ hours.")
                countdown(0.25 * 3600 - 15)
                return False

            else:
                kiss = emoji.emojize(":kiss_mark:")
                send_discord_message.send_discord_message(f"Until next thursday! Enjoy Pro Series. Big kisses to <@239792809954115584>.{kiss}{kiss}{kiss}")
                countdown(24 * 3600)
                return True
        else:
            countdown(3600)

#while SIGN_UPS_OPEN == False:
#    SIGN_UPS_OPEN = check_time_and_sleep()

RACE_ID = 165485
#send_discord_message.send_discord_message("Yo Guys, Dalking here, did you know I won the first ever Pro Series race ?")
#FORGOT = check_time_and_sleep(FORGOT)
FORGOT = False
while True and not FORGOT:
    inside, out_signed, out_elo, drivers, cut_off = (find_drivers_split1(RACE_ID))
    inside = f"In This Week's Pro Series Split 1:\n```\n{inside[:-2]}```\nDrivers in Split 1: {drivers}\nCut-Off is: **{cut_off}**"
    print(inside)
    cars = car_counts.get_cars(RACE_ID)
    out = f"{out_signed}\n{out_elo}\n{cars}"
    print(out)

    try:
        inside2, out_signed2, out_elo2, drivers2, cut_off2 = (find_drivers_split2(RACE_ID, cut_off))
        inside2 = f"In This Week's Pro Series Split 2:\n```\n{inside2[:-2]}```\nDrivers in Split 2: {drivers2}\nCut-Off is: **{cut_off2}**"
        print(inside2)
        out2 = f"{out_signed2}\n{out_elo2}"
        print(out2)

        logging.info(f"{inside}\n{out}\n——————————————————————————\n{inside2}\n{out2}")
        send_discord_message.send_discord_message(f"{inside}\n{out}\n——————————————————————————\n{inside2}\n{out2}")
    except:
        logging.info(f"{inside}\n{out}\n——————————————————————————\nNo Split 2?")
        send_discord_message.send_discord_message(f"{inside}\n{out[:-2]}\n——————————————————————————\nNo Split 2?")

    FORGOT = check_time_and_sleep(FORGOT)