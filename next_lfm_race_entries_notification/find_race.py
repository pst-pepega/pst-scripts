import pandas as pd
from datetime import datetime

# Read data from CSV file
df = pd.read_csv('weather_data.csv')

# Convert 'Date and Time' column to datetime
df['Date and Time'] = pd.to_datetime(df['Date and Time'])

# Get the current date
current_date = pd.to_datetime('today')

# Filter data for dates after the current date
future_data = df[df['Date and Time'] >= current_date]

# Find the next race with effective temperature of 28 and up
next_race = future_data[future_data['Avg Temp'] >= 28]['Date and Time'].min()

if not pd.isnull(next_race):
    print(f"The next race with effective temperature of 28 and up is scheduled for: {next_race}")
else:
    print("There is no upcoming race with effective temperature of 28 and up.")
