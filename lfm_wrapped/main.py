import requests
import json
from collections import defaultdict
from datetime import datetime, timedelta
import logging

# Constants
USER_ID = 1704
NAME = "Kylo"
BASE_URL = "https://api3.lowfuelmotorsport.com/api"
USER_RACES_ENDPOINT = f"{BASE_URL}/users/getUsersPastRaces/{USER_ID}"
RACE_DETAILS_ENDPOINT = f"{BASE_URL}/race/{{race_id}}"

print(USER_RACES_ENDPOINT)

# Fetch user races
def fetch_user_races():
    params = {"start": 0, "limit": 1000, "event": "false", "season": "false"}
    response = requests.get(USER_RACES_ENDPOINT, params=params)
    response.raise_for_status()
    return response.json()

# Fetch race details
def fetch_race_details(race_id):
    response = requests.get(RACE_DETAILS_ENDPOINT.format(race_id=race_id))
    response.raise_for_status()
    return response.json()


# Helper function to format seconds into hours:minutes:seconds
def format_time(total_seconds):
    td = timedelta(seconds=total_seconds)
    hours, remainder = divmod(td.seconds, 3600)
    minutes, seconds = divmod(remainder, 60)
    return f"{hours + td.days * 24:02}:{minutes:02}:{seconds:02}"

# Process races
def process_races(races):
    stats = {
        "event_name_counts": defaultdict(int),
        "track_name_counts": defaultdict(int),
        "car_name_counts": defaultdict(int),
        "sim_id_counts": defaultdict(int),
        "time_by_track_and_car": defaultdict(lambda: defaultdict(float)),
        "laps_by_track_and_car": defaultdict(lambda: defaultdict(int)),
        "total_sr_change": 0,
        "total_ratingGain": 0,
        "total_incidents": 0,
    }

    for race in races:
        race_date = datetime.strptime(race["race_date"], "%Y-%m-%d %H:%M:%S")
        print(race_date)
        if race_date.year != 2024:
            continue

        stats["event_name_counts"][race["event_name"]] += 1
        stats["track_name_counts"][race["track_name"]] += 1
        stats["car_name_counts"][race["car_name"]] += 1
        stats["sim_id_counts"][race["sim_id"]] += 1

        # Fetch race details
        race_details = fetch_race_details(race["race_id"])

        #print(race_details.get("race_results", []))

        # Find user results in race

        try: 
            user_result = next((
                driver 
                for driver in race_details["race_results"].get("OVERALL", [])
                if driver["user_id"] == USER_ID
            ), None)

            if user_result:
                # Sum time (convert to seconds)
                time_str = user_result.get("time", "0:00:00.000")
                hours, minutes, seconds = map(float, time_str.split(":"))
                total_seconds = hours * 3600 + minutes * 60 + seconds
                stats["time_by_track_and_car"][race["track_name"]][race["car_name"]] += total_seconds

                # Sum laps
                stats["laps_by_track_and_car"][race["track_name"]][race["car_name"]] += user_result.get("laps", 0)

            # Sum other metrics
            stats["total_sr_change"] += float(race.get("sr_change", 0))
            stats["total_ratingGain"] += int(race.get("rating_change", 0))
            stats["total_incidents"] += int(race.get("incidents", 0))
        except Exception as e:
            logging.info(e)

    
        #print(stats)

    return stats

# Main execution
def main():
    user_races = fetch_user_races()
    print(user_races[0])
    stats = process_races(user_races)


    # Sort dictionaries by descending values
    stats["event_name_counts"] = dict(sorted(stats["event_name_counts"].items(), key=lambda x: x[1], reverse=True))
    stats["track_name_counts"] = dict(sorted(stats["track_name_counts"].items(), key=lambda x: x[1], reverse=True))
    stats["car_name_counts"] = dict(sorted(stats["car_name_counts"].items(), key=lambda x: x[1], reverse=True))
    stats["sim_id_counts"] = dict(sorted(stats["sim_id_counts"].items(), key=lambda x: x[1], reverse=True))

    stats["total_sr_change"] = round(stats["total_sr_change"],3)

    # Convert nested defaultdicts to sorted dicts
    stats["time_by_track_and_car"] = {
        track: dict(sorted(cars.items(), key=lambda x: x[1], reverse=True))
        for track, cars in stats["time_by_track_and_car"].items()
    }
    stats["laps_by_track_and_car"] = {
        track: dict(sorted(cars.items(), key=lambda x: x[1], reverse=True))
        for track, cars in stats["laps_by_track_and_car"].items()
    }

    # Convert total time to formatted string
    for track, cars in stats["time_by_track_and_car"].items():
        for car, total_seconds in cars.items():
            stats["time_by_track_and_car"][track][car] = format_time(total_seconds)

    print(f"LFM WRAPPED FOR: {NAME}")

    # Print stats
    print(json.dumps(stats, indent=4, ensure_ascii=False))

if __name__ == "__main__":
    main()
