# PST scripts

The place where the ultimate 5HEAD write scripts that have no purpose :)

## pst-lfm script usage

Install requirements

```sh
pip install -r requirements.txt
```

Create the config file `config.json`

```
{
    "discordToken": "<token>",
    "users": [
        "id": 12345,
        "name": "John Wick"
    ]
}
```

Run the script

```sh
python pst-lfm.py
```

### Optional: Install the timer

You can use the Systemd timer to run the script at a set interval

```sh
sudo cp pst-lfm.service pst-lfm.timer /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable --now pst-lfm.timer
```

