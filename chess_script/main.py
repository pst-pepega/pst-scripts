import requests
import time
import send_discord_message
import logging

# Create a logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'
}


def get_user_stats(username):
  url = f"https://api.chess.com/pub/player/{username}/stats"
  retries = 3  # Number of retries before giving up
  wait_time = 1  # Initial wait time in seconds

  for attempt in range(retries + 1):
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
      # Check for success message (optional, might be fragile)
      if "Just a moment..." not in response.text:
        return response.json()
      else:
        print(f"Attempt {attempt+1}/{retries}: Waiting for data...")
    else:
      print(f"Error fetching stats for user {username}: {response.status_code}")
      return None

    # Increase wait time for subsequent retries (exponential backoff)
    wait_time *= 2
    time.sleep(wait_time)

  print(f"Failed to retrieve stats for user {username} after {retries} attempts.")
  return None

def save_stats(username, stats):
  with open(f"{username}_stats.txt", "w") as f:
    f.write(str(stats))

def load_stats(username):
  try:
    with open(f"{username}_stats.txt", "r") as f:
      return eval(f.read())
  except FileNotFoundError:
    return None

def calculate_changes(old_stats, new_stats):
  changes = {}
  for time_control in ["rapid", "bullet", "blitz"]:
    try:
      old_rating = old_stats[f'chess_{time_control}']['last']['rating']
      old_record = old_stats[f'chess_{time_control}']['record']

      new_rating = new_stats[f'chess_{time_control}']['last']['rating']
      new_record = new_stats[f'chess_{time_control}']['record']

      change_rating = new_rating - old_rating
      old_games = old_record['win'] + old_record['loss'] + old_record['draw']
      new_games = new_record['win'] + new_record['loss'] + new_record['draw']
      print(old_games, new_games)
      absolute_change_wins = new_record['win'] - old_record['win']
      absolute_change_draw = new_record['draw'] - old_record['draw']
      absolute_change_loss = new_record['loss'] - old_record['loss']
      changes[time_control] = [change_rating, absolute_change_wins, absolute_change_draw, absolute_change_loss, old_rating, new_rating]
    except:
      print(f"Cant find {time_control}")
  print(changes)
  return changes

def main():
  usernames = ["kylooooooooo", "shftctrl", "teemukarppinen", "deomors", "ukogmonkey"]  # Add more usernames here
  for username in usernames:
    result = ""
    new_stats = get_user_stats(username)
    old_stats = load_stats(username)
    
    changes = calculate_changes(old_stats or {}, new_stats)

    result += (f"**Stats for {username}:**\n")
    for time_control, data in changes.items():
      print(data)
      if data[0] != 0:
          result += (f"\t{time_control.upper()}: Rating Change: **{data[0]}** [{data[4]}->{data[5]}] — Wins/Draws/Losses: {data[1]}/{data[2]}/{data[3]}\n")

    save_stats(username, new_stats)
    if result != (f"**Stats for {username}:**\n"):
      logger.info(result)
      send_discord_message.send_discord_message(result)

if __name__ == "__main__":
  main()
