#!/bin/bash

export PY_PYTHON=3.12
alias python='python3.12'
alias py='python3.12'


cd /home/luca/pst-scripts/bop_notifications || { echo "Failed to change directory"; exit 1; }
pwd >> /home/luca/pst-scripts/bop_notification.log
echo $PWD

sleep 10
python3 /home/luca/pst-scripts/bop_notifications/main.py > bop_notification.log
current_date=$(date +"%Y-%m-%d %H:%M:%S")
git add bop_scores.csv laps_data.csv all_bop.json
git commit -m "new bop data [$current_date]"
