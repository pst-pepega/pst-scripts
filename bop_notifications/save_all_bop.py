import json


def add_to_all_bop(relevant_laps, other_laps, bop_dict):
    # Iterate over the elements of the JSON list
    for entry in relevant_laps:
        car_name = entry['car_name']
        bop = entry['bop']
        
        # Check if the car_name already exists in the dictionary
        if car_name in bop_dict:
            # Append bop to the existing list of bop values for the car_name
            bop_dict[car_name] += int(bop)
        else:
            # Create a new list with the bop value for the car_name
            bop_dict[car_name] = int(bop)
                
    # Iterate over the elements of the JSON list
    for entry in other_laps:
        car_name = entry['car_name']
        bop = entry['bop']
        
        # Check if bop is greater than 0
        if int(bop) > 0:
            # Check if the car_name already exists in the dictionary
            if car_name in bop_dict:
                # Append bop to the existing list of bop values for the car_name
                bop_dict[car_name] += int(bop)
            else:
                # Create a new list with the bop value for the car_name
                bop_dict[car_name] = int(bop)

    # Print the resulting dictionary
    print(bop_dict)
    return bop_dict


def save_all_bop(bop_dict, file_path = "all_bop.json"):
    # File path to save the dictionary

    # Save the dictionary to a JSON file
    with open(file_path, 'w') as file:
        json.dump(bop_dict, file)


def return_all_bop_value(bop_dict, file_path = "all_bop.json"):
    result = "**Average BOP**```diff\n"
        # Load the dictionary from the JSON file
    with open(file_path, 'r') as file:
        loaded_bop_dict = json.load(file)


    for car_name, value in bop_dict.items():
        if car_name in loaded_bop_dict:
            loaded_value = loaded_bop_dict[car_name]
            if loaded_value < value:
                result += f"- {car_name} BOP changed from {loaded_value} to {value} [Δ{round(value - loaded_value, 3)}]\n"
            elif loaded_value > value:
                result += f"+ {car_name} BOP changed from {loaded_value} to {value} [Δ{round(value - loaded_value, 3)}]\n"
        else:
            result += f"- {car_name} BOP changed from N/A to {value} [ΔNEW]"

    result += "```"
    return result