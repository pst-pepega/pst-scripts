import datetime
from datetime import datetime
from logging import config, info
import logging
import requests
import requests
import json
from typing import List, Any
import os
from dotenv import load_dotenv
import emoji

import date_time_difference_and_record

DISCORD_API = "https://discord.com/api"
DISCORD_CHANNEL_ID = "1015601499117727856"
RWAGGY_CHANNEL_ID = 1317955387592284
RWAGGY_ID = "285864117766455299"
LFM_API = "https://api2.lowfuelmotorsport.com/api"


# Load variables from .env file
load_dotenv()

# Get the Discord token from environment variables
discord_token = os.getenv("discord_token")


# you figure discord token out
def send_discord_message(content, channel = DISCORD_CHANNEL_ID, token = discord_token):
    url = f"{DISCORD_API}/v9/channels/{channel}/messages"
    headers = {
            "Authorization": token,
            "Content-Type": "application/json"
            }

    data = { "content": str(content) }

    response = requests.post(url, headers=headers, data=json.dumps(data))

    if response.status_code == 200:
        print("Request successful!")
    else:
        print("Request failed with status code:", response.status_code, response.text)


def get_status():

    response = requests.get(f"{LFM_API}/accstatus")

    if response.status_code != 200:
        return

    # Parse the JSON response
    data = response.json()
    text = data.get("text")
    if not text:
        return

    if "OFFLINE" in text:
        return "down"
    else:
        return "up"
    

def configure_message(user):
    content = f"<@{user}>, your pi crashed again.\n"
    content += "https://tenor.com/view/walter-white-walter-crying-walter-dying-gif-20964719 \n"
    content += "https://tenor.com/view/cigar-smoking-smoke-back-off-gif-17958464 \n"    
    content += f"{emoji.emojize(':warning:')}{emoji.emojize(':warning:')}{emoji.emojize(':warning:')}{emoji.emojize(':warning:')}{emoji.emojize(':warning:')}{emoji.emojize(':warning:')}{emoji.emojize(':warning:')}{emoji.emojize(':warning:')}{emoji.emojize(':warning:')}{emoji.emojize(':warning:')}"
#    content += "⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠"
    return content

down_time = 'down_date_time.txt'
down_difference = 'max_downtime.txt'

up_time = 'up_date_time.txt'
up_difference = 'max_uptime.txt'
status = "up"
status_was = ""
with open('status.txt', 'r') as file:
    status_was = file.read()
#status_was = status
status = get_status()

if status == "down" and status_was == "up":
    content = configure_message(RWAGGY_ID)
    send_discord_message(content)
    try:
        send_discord_message("Your fucking ~~game is dead~~ server browser is down again, boss.", RWAGGY_CHANNEL_ID)
    except Exception as e:
        with open('exceptions.txt', 'a') as file:
            file.write(e)
    with open('status.txt', 'w') as file:
        file.write("down")

    # Retrieve up datetime and calculate difference with now
    saved_datetime = date_time_difference_and_record.get_datetime_from_file(up_time)
    logging.info(f"Last downtime: {saved_datetime}")

    # Save current datetime for UP
    date_time_difference_and_record.save_current_datetime(up_time)
    # Save current datetime for DOWN
    date_time_difference_and_record.save_current_datetime(down_time)

    if saved_datetime:
        new_difference = date_time_difference_and_record.calculate_difference(saved_datetime, datetime.now())
        logging.info(f"{new_difference} seconds since last time was server went from down -> up.")
        
        # Update the difference file if the new difference is larger
        if date_time_difference_and_record.update_difference_if_larger(up_difference, new_difference):
            readable_time = date_time_difference_and_record.convert_difference_to_readable_format(new_difference)
            content = f"ACC servers hit a record-breaking {readable_time} of uptime—shame it’s over already, as the mandatory nap time begins anew."
        else:
            readable_time = date_time_difference_and_record.convert_difference_to_readable_format(new_difference)
            content = f"ACC Servers were up for {readable_time}. https://media1.tenor.com/m/8CPvdgIHcZ4AAAAC/rookie-numbers.gif"

        send_discord_message(f"The server browser was up for {readable_time}.", RWAGGY_CHANNEL_ID)

        send_discord_message(content, DISCORD_CHANNEL_ID)
        #send_discord_message(content, 770245335578050591)
        

if status == "up" and status_was == "down":
    content = "Thank you Rwaggy, Jesus loves you."
    send_discord_message(content)
    send_discord_message("Yea thanks mate, server browser is fixed.", RWAGGY_CHANNEL_ID)
    with open('status.txt', 'w') as file:
        file.write("up")

    # Retrieve up datetime and calculate difference with now
    saved_datetime = date_time_difference_and_record.get_datetime_from_file(down_time)

    # Save current datetime for UP
    date_time_difference_and_record.save_current_datetime(up_time)
    # Save current datetime for DOWN
    date_time_difference_and_record.save_current_datetime(down_time)

    if saved_datetime:
        new_difference = date_time_difference_and_record.calculate_difference(saved_datetime, datetime.now())
        
        # Update the difference file if the new difference is larger
        if date_time_difference_and_record.update_difference_if_larger(down_difference, new_difference):
            readable_time = date_time_difference_and_record.convert_difference_to_readable_format(new_difference)
            content = f"After being offline for {readable_time}, the ACC servers have finally woken up—let’s see how long they stay awake this time!"
            send_discord_message(f"It was down for {readable_time}. Mate. Whoever is in charge of that mess. Sort them out.",RWAGGY_CHANNEL_ID)
        else:
            readable_time = date_time_difference_and_record.convert_difference_to_readable_format(new_difference)
            content = f"ACC Servers were down for {readable_time}. https://media1.tenor.com/m/hF7VpP0MZkIAAAAC/it-is-acceptable-gus-fring-acceptable.gif"
            send_discord_message(f"It was down for {readable_time}. https://media1.tenor.com/m/hF7VpP0MZkIAAAAC/it-is-acceptable-gus-fring-acceptable.gif", RWAGGY_CHANNEL_ID)


        send_discord_message(content, DISCORD_CHANNEL_ID)
        #send_discord_message(content, 770245335578050591)