from __future__ import annotations
import requests
from datetime import datetime
from dateutil import tz
import pickle
import json
import dataclasses
from typing import List, Any


DISCORD_API = "https://discord.com/api"
PST_CHANNEL_ID = 1015601499117727856
LFM_API = "https://api2.lowfuelmotorsport.com/api"

@dataclasses.dataclass
class LFMUsers():
    id: int
    name: str

@dataclasses.dataclass
class Config():
    discord_token: str
    users: list[LFMUsers]

    @classmethod
    def load_config(cls, config_file: str) -> Config:
        with open(config_file, "r") as user_fp:
            config = json.load(user_fp)
            discord_token = config["discordToken"]
            users = []
            for user in config["users"]:
                users.append(LFMUsers(user["id"], user["name"]))

        return cls(discord_token, users)

def load_previous_races_ids(file_path: str) -> dict:
    try:
        with open(file_path, "rb") as file:
            return pickle.load(file)
    except FileNotFoundError:
        return {}

config = Config.load_config("config.json")

def send_discord_message(content, token: str = config.discord_token):
    url = f"{DISCORD_API}/v9/channels/{PST_CHANNEL_ID}/messages"
    headers = {
            "Authorization": token,
            "Content-Type": "application/json"
            }

    data = { "content": str(content) }

    response = requests.post(url, headers=headers, data=json.dumps(data))

    if response.status_code == 200:
        print("Request successful!")
    else:
        print("Request failed with status code:", response.status_code, response.text)

def get_users_past_races(user_id: int) -> List[dict[str, Any]]:

    url = f"{LFM_API}/users/getUsersPastRaces/{user_id}?start=0&limit=5"
    response = requests.get(url)
    return response.json()

def check_if_race_in_db(db: dict, user_id: int, race_id: int) -> bool:

    if user_id not in db.keys():
        db[user_id] = race_id
        return False

    is_present = db[user_id] == race_id
    if not is_present:
        db[user_id] = race_id
    
    return is_present

def get_race_time(race_data: dict) -> datetime:

    race_date = datetime.strptime(race_data["race_date"], "%Y-%m-%d %H:%M:%S")

    race_date = race_date.replace(tzinfo=tz.tzstr("UTC+1"))
    race_date_local = race_date.astimezone(tz.tzlocal())

    return race_date_local

def check_last_race(db: dict, user: LFMUsers) -> None:

    past_races = get_users_past_races(user.id)

    if len(past_races) == 0:
        print(f"never raced")
        return

    last_race = past_races[0]
    race_id = last_race["race_id"]
    print(f"Last race ID: {race_id}")
    if check_if_race_in_db(db, user.id, race_id):
        print("Already seen that race")
        return

    race_time = get_race_time(last_race)
    race_time_epoch = int(race_time.timestamp())
    position_delta = last_race['start_pos'] - last_race['finishing_pos']
    sr_change_diff = "+ 📈" if last_race['sr_change'] > 0 else "- 📉" if last_race['sr_change'] < 0 else "∆"
    elo_change_diff = "+ 📈" if last_race['rating_change'] > 0 else "- 📉" if last_race['rating_change'] < 0 else "∆"
    position_change_diff = "+ 📈" if position_delta > 0 else "- 📉" if position_delta < 0 else "∆"
    best_of_week = ""
    if last_race["best_of_week"] == "true" : best_of_week = f"<:AYAYA:1102246176645988393>  Best result of the week for {user.name} <:AYAYA:1102246176645988393>"


    message = f"""
Latest LFM race for {user.name}
{last_race['event_name']} in the {last_race['car']} at {last_race['track_name']}
{race_time}
{position_change_diff} P{last_race['start_pos']} -> P{last_race['finishing_pos']} [{position_delta}]
{elo_change_diff} {last_race['rating_change']} Elo
{sr_change_diff} {last_race['sr_change']} SR [{last_race['incidents']} IP]
{best_of_week}
"""

    print(message)

    discord_message = f"""
### Latest LFM race for {user.name}
**{last_race['event_name']} in the {last_race['car']} at {last_race['track_name']}**
<t:{race_time_epoch}:R>
```diff
{position_change_diff} P{last_race['start_pos']} -> P{last_race['finishing_pos']} [{position_delta}]
{elo_change_diff} {last_race['rating_change']} Elo
{sr_change_diff} {last_race['sr_change']} SR [{last_race['incidents']} IP]
{best_of_week}
```"""

    data = requests.get(f"https://api2.lowfuelmotorsport.com/api/users/getUserStats/{user.id}").json()
    
    podiums = data["podium"]
    wins = data["wins"]
    poles = data["poles"]
    races = data["starts"]
    
    if last_race['finishing_pos'] == 1:
        discord_message += (f"\nCongratulations on your {get_ordinal(wins)} victory! <:peepoheart:1112477859680899122> Well done!")
        #send_discord_message("Congratulations on your victory <@{user.discord_id}! <:peepoheart:1112477859680899122> Well done!")
    elif last_race['finishing_pos'] <= 3:
        discord_message += (f"\nYou've clinched a spot on the podium! <:WICKED:1015967738985979925> Way to go, your {get_ordinal(podiums)}, congrats!")
        #send_discord_message("You've clinched a spot on the podium! <@{user.discord_id}! <:WICKED:1015967738985979925> Way to go, congrats!")

    if last_race['start_pos'] == 1:
        discord_message += (f"\n{get_ordinal(poles)} Pole Position, Let's go! Ayrton Senna would be proud! <:Binoculars:1101918533811707914>")

    #send_discord_message(calculate_custom_milestone(races), config.discord_token)

    discord_message += f"\n{calculate_custom_milestone(races)}"

    send_discord_message(discord_message, config.discord_token)
            
    
def get_ordinal(number):
    if 10 <= number % 100 <= 20:
        suffix = "th"
    else:
        suffix = {1: "st", 2: "nd", 3: "rd"}.get(number % 10, "th")
    
    return str(number) + suffix

def calculate_custom_milestone(current_races):
    if current_races <= 50:
        milestone_interval = 10
    elif current_races <= 100:
        milestone_interval = 20
    elif current_races <= 500:
        milestone_interval = 25
    elif current_races <= 1000:
        milestone_interval = 50
    else:
        milestone_interval = 100
    
    next_milestone = ((current_races // milestone_interval) + 1) * milestone_interval
    races_to_next_milestone = next_milestone - current_races
    
    message = f"{races_to_next_milestone} race(s) to your next Milestone! Keep it up!"
    
    # Check if a milestone has been achieved
    if current_races % milestone_interval == 0:
        message = f"You've just achieved a milestone of {current_races} races!\n" + message
    
    return message

def main():
    
    race_ids = load_previous_races_ids("race_ids.pkl")

    for user in config.users:
        print(f"User {user.name} (id: {user.id})")
        check_last_race(race_ids, user)
        print("-" * 20)

    # Save race_ids to file
    with open("race_ids.pkl", "wb") as file:
        pickle.dump(race_ids, file)


if __name__ == "__main__":
    main()

