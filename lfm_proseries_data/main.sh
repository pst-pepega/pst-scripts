#!/bin/bash

# CONSTANTS
current_season=16

cp lfm_proseries_data.csv old.csv
python3 proseriesdata.py $1
python3 aggregate_data.py

python3 aggregate_data_for_season.py $current_season
python3 compare_old_to_new_pro_series_data.py > differences.csv

mv lfm_proseries_data_pro-series-s$current_season.csv lfm_proseries_data_current_season.csv
echo "renamed season file."
rm old.csv

git add race_results.csv lfm_proseries_data.csv lfm_proseries_data_current_season.csv
git commit -m "added data for pro series race $1 to the aggregate and to aggregate for season $current_season"
