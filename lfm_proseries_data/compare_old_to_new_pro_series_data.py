import csv

def compare_csv(file1, file2):
    # Load data from the first file
    with open(file1, 'r', encoding='utf-8') as f1:
        reader1 = list(csv.DictReader(f1))
    
    # Load data from the second file
    with open(file2, 'r', encoding='utf-8') as f2:
        reader2 = list(csv.DictReader(f2))
    
    # Index data by user_id, vorname, and nachname
    key_fields = ['vorname', 'nachname', 'user_id']
    data1 = {tuple(row[field] for field in key_fields): row for row in reader1}
    data2 = {tuple(row[field] for field in key_fields): row for row in reader2}
    
    # Determine maximum field lengths for alignment
    max_name_len = max(len(f"{row['vorname']} {row['nachname']}") for row in reader1 + reader2)
    max_field_len = max(len(field) for field in reader1[0].keys())
    
    # Find common keys and compare rows
    common_keys = set(data1.keys()) & set(data2.keys())
    for key in common_keys:
        row1 = data1[key]
        row2 = data2[key]
        vorname, nachname, user_id = key
        name = f"{vorname} {nachname}"
        for field in row1.keys():
            if field in row2 and row1[field] != row2[field]:
                try:
                    # Calculate numeric difference
                    diff = float(row2[field]) - float(row1[field])
                    print(f"{name:<{max_name_len}} {field:<{max_field_len}} {diff:+.2f}")
                except ValueError:
                    # Handle non-numeric changes
                    print(f"{name:<{max_name_len}} {field:<{max_field_len}} changed from {row1[field]} to {row2[field]}")

# Input files
file2 = 'lfm_proseries_data.csv'
file1 = 'old.csv'

# Run comparison
compare_csv(file1, file2)
