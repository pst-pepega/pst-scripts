import requests
import pandas as pd
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import logging
import json
import datetime
import send_discord_message
import time

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)
LFM_API = "https://api2.lowfuelmotorsport.com/api"
FILE_PATH = f"lap_ids.txt"


def get_track_id(track_name):

    track_list_response = requests.get(f"{LFM_API}/lists/getTracks")

    # Check if the request was successful (status code 200)
    if track_list_response.status_code != 200:
        logger.warn("Failed to get list of track IDs from LFM API")
        return None

    data = track_list_response.json()

    # Filter tracks with track_id between 124 and 155
    filtered_tracks = [track for track in data if 124 <= track['track_id'] <= 155]

    # Create a dictionary with track_name as keys and track_id as values
    track_dict = {track['track_name']: track['track_id'] for track in filtered_tracks}

    # Convert track_dict keys to a list of strings
    track_list = list(track_dict.keys())

    # Find the closest matching track name
    closest_track = find_closest_track(track_list, track_name)

    # Get the corresponding track_id
    track_id = track_dict.get(closest_track)

    return track_id


def find_closest_track(match_tracks, input_track):
    closest_match, _ = process.extractOne(input_track, match_tracks, scorer=fuzz.partial_ratio)
    return closest_match


def get_all_tracks():

    # Make a GET request to the API
    response = requests.get(f"{LFM_API}/lists/getTracks")

    # Check if the request was successful (status code 200)
    if response.status_code != 200:
        logger.warning("Failed to get track ID from LFM API")
        return None

    # Parse the JSON response
    data = response.json()

    # Filter tracks with track_id between 124 and 155
    filtered_tracks = [track for track in data if 124 <= track['track_id'] <= 155]

    # Create a dictionary with track_name as keys and track_id as values
    track_dict = {track['track_name']: track['track_id'] for track in filtered_tracks}

    # Convert track_dict keys to a list of strings
    track_list = list(track_dict.keys())

    return track_list


def get_bop(track):
    
    logger.info(track)
    # URL to fetch JSON data from
    url = f'https://api2.lowfuelmotorsport.com/api/hotlaps/getBopPrediction?track={get_track_id(track)}&class=GT4'
    logger.info(url)
    try:
        # Send an HTTP GET request to the URL and parse the JSON response
        data = requests.get(url).json()
        target = {data['bopdata']['target_time']}

    except Exception as e:
        print(f'An error occurred: {e}')

    return str(list(target)[0])


def find_closest_acc_track(match_tracks, input_track):
    closest_match, _ = process.extractOne(input_track, match_tracks, scorer=fuzz.partial_ratio)
    return closest_match


def time_to_seconds(time_str):
    # Split the time string into minutes, seconds, and milliseconds
    minutes, seconds = time_str.split(':')
    
    # Calculate the total time in seconds
    total_seconds = int(minutes) * 60 + float(seconds)
    
    return total_seconds


# Function to read lap_ids from a file and return a list
def read_lap_ids_from_file(file_path = FILE_PATH):
    with open(file_path, 'r') as file:
        lap_ids = [line.strip() for line in file.readlines()]
    return lap_ids


# Function to write a list of lap_ids to a file
def write_lap_ids_to_file(lap_ids, file_path = FILE_PATH):
    with open(file_path, 'w') as file:
        for lap_id in lap_ids:
            file.write(f"{lap_id}\n")


