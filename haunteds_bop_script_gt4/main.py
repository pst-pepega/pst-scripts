import requests
import pandas as pd
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import logging
import json
import datetime
import send_discord_message
import bop
import time

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)
FILE_PATH = "lap_ids.txt"
BOP_ALERT_CHANNEL = 1185878849641259028
FOCH_DISCORD_ACCCCC = 770245335578050591

# Initialize an empty list to store lap_ids
lap_ids_list = []

# Read lap_ids from the file
lap_ids_list = bop.read_lap_ids_from_file(FILE_PATH)

logger.info(lap_ids_list)

track_list = bop.get_all_tracks()
for track in track_list:
    target = bop.get_bop(track)
    logger.info(target)
    target = bop.time_to_seconds(target)
    logger.info(target)


    url = f'https://api2.lowfuelmotorsport.com/api/hotlaps?track={bop.get_track_id(track)}&class=gt4&car=&major=&version=&source=bop'
    logger.info(url)
    try:
        # Send an HTTP GET request to the URL and parse the JSON response
        data = requests.get(url).json()

    except Exception as e:
        print(f'An error occurred: {e}')

    for lap in data['data']:
        laptime = bop.time_to_seconds(lap['lap'])
        if(laptime >= target):
            continue
        else:
            if(str(lap['lap_id']) in lap_ids_list):
                logger.info("Lap id already posted.")
            else:
                message = f"{lap['vorname']} {lap['nachname']} did a {lap['lap']} with the {lap['car_name']} at {lap['track_name']} on {lap['created_at']} with {lap['ballast']} ballast."
                logger.info(message)
                try:
                    send_discord_message.send_discord_message(message, BOP_ALERT_CHANNEL)
                    send_discord_message.send_discord_message(f":warning: :warning: :warning: \n The BOP Police has found a suspect. Please ping Haunted to make sure he is aware of this glaring issue. {message} \n :warning: :warning: :warning:", FOCH_DISCORD_ACCCCC)
                    lap_ids_list.append(lap['lap_id'])
                except:
                    logger.error("Message could not sent.")

bop.write_lap_ids_to_file(lap_ids_list)